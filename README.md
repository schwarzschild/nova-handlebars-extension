<!--
👋 Hello! As Nova users browse the extensions library, a good README can help them understand what your extension does, how it works, and what setup or configuration it may require.

Not every extension will need every item described below. Use your best judgement when deciding which parts to keep to provide the best experience for your new users.

💡 Quick Tip! As you edit this README template, you can preview your changes by selecting **Extensions → Activate Project as Extension**, opening the Extension Library, and selecting "Handlebars" in the sidebar.

Let's get started!
-->

<img src="https://gitlab.com/schwarzschild/nova-handlebars-extension/-/raw/main/Images/extension/preview.png" alt="Preview" width="856"/>

**Handlebars** provides syntax highlighting and autocompletion for [{{ Handlebars }}](https://handlebarsjs.com/) and [{{ Mustache }}](https://mustache.github.io/).

## Language Support

<!--
🎈 Whether your extension covers the entirety of a language's syntax or a subset, it can be helpful to describe that for users:
-->

Handlebars currently supports the following features:

- Syntax highlighting
- Basic completion

Support for

- Auto closing

is planned for a future update.
